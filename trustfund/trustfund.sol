// @title A contract that acts as an interface to the Security Innovation CTF "Trust Fund"
// @author Cole Nixon

pragma solidity ^0.4.24;
contract TrustFund {
    function withdraw() external;
    function ctf_challenge_add_authorized_sender(address _addr) external;
}
